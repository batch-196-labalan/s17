// alert("hi");

// FUNCTIONS
/*
	Syntax:
		function functionName(){
			code block(statement)
		}
*/

function printName(){
	console.log('My name is Ana');
};
// invoke/ call the function
printName();


// FUNCTION DECLARATION VS EXPRESSIONS
// can invoke before or after

declaredFunction();

function declaredFunction(){
	console.log("Hi I am from declaredFunction()");
};

declaredFunction();





// FUNCTION EXPRESSION (cannot invoke/hoiset before)
	// Anonymous function - function without a name

let variableFunction = function(){
	console.log("I am from variable function");
};
variableFunction();


let funcExpression = function funcName(){
	console.log('Hello from the the other side');
}
funcExpression();



// Can reassign declared functions and function expressions to new anonymous function

declaredFunction = function(){
	console.log("updated declaredFunction");
};

declaredFunction();

funcExpression = function(){
	console.log('updated funcExpression');
}
funcExpression();

const constantFunction = function(){
	console.log('Initialized with const');
}

constantFunction();
// reassignment w/ constant expression is not possible





// FUNCTION SCOPING
/*
	Javascript variables 3 types of scope:
	1. local/block
	2. global
	3. function

*/

// {
// 		let localVar = "Armando Perez";
// }

let globalVar = "Mr. worldwide";
console.log(globalVar);

// console.log(localVar)

		// global to become accessible
// var functionVar = 'Joe';
// 	const functionConst = 'Nick';
// 	let functionLet = 'Kevin';

function showNames(){

	// Function scope variable
	var functionVar = 'Joe';
	const functionConst = 'Nick';
	let functionLet = 'Kevin';

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};
showNames();

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);





// NESTED FUNCTION

function myNewFunction(){
	let name = 'Yor';

	function nestedFunction(){
	   let nestedName = 'Brando';
	   console.log(nestedName);
	};
	nestedFunction();
};
myNewFunction();




// FUNCTION AND GLOBAL SCOPE VARIABLE (function scope cannot be put outside)

let globalName = 'Alan';

function myNewFunction2(){
	let nameInside = "Marco";
	console.log(globalName);
};
myNewFunction2();



// // ALERT

// alert('hello world');

// function showSampleAlert(){
// 	alert('hello user');
// };
// showSampleAlert();




// PROMPT
/*
	Syntax:
		prompt("<dialog>")

*/

// let samplePrompt = prompt("enter your name:");
// console.log("Hello " + samplePrompt);

// let sampleNullPrompt = prompt("Don't enter anything");
// console.log(sampleNullPrompt);

function printWelcomeMessage(){
	let firstName = prompt('Enter your first name:');
	let lastName = prompt('Enter your last name:');

	console.log('Hello, ' + firstName + " " + lastName + "!");
	console.log('Welcome to my page');
};

printWelcomeMessage();

// FUNCTION NAMING CONVENTIONS

function getCourses(){
	let courses = ['Science 101,', 'Arithmetic 103', 'Grammar 105'];
	console.log(courses);
};
getCourses();


// avoid generic names
function get(){
	console.log(name);
};
get();


// avoiud pointless function names
function foo(){
	console.log(25 % 5);
}
foo();


// name functions in small caps
function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
}
displayCarInfo();